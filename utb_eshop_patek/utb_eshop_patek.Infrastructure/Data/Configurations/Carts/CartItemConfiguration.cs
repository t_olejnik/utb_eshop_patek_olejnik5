﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Carts;

namespace utb_eshop_patek.Infrastructure.Data.Configurations.Carts
{
    public class CartItemConfiguration : IEntityTypeConfiguration<CartItem>
    {
        public void Configure(EntityTypeBuilder<CartItem> builder)
        {
            builder.ToTable("CartItems", "Web");
            builder.HasKey(e => e.ID);
            builder.HasOne(e => e.Product).WithMany().IsRequired().HasForeignKey(e => e.ProductID).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
