﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Domain.Entities
{
    public abstract class Entity
    {
        public int ID { get; set; }
        public DateTime DateCreated { get; set; }
        public int CreatedBy { get; set; }
        public DateTime DateUpdated { get; set; }
        public int UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
