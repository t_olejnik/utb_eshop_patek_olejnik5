﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Addresses;

namespace utb_eshop_patek.Domain.Entities.Orders
{
    public class Order : Entity
    {
        public int UserID { get; set; }
        public string OrderNumber { get; set; }
        public int? ShippingAddressID { get; set; }
        public int? BillingAddressID { get; set; }
        public IList<OrderItem> OrderItems { get; set; }
        public Address ShippingAddress { get; set; }
        public Address BillingAddress { get; set; }
    }
}
