﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Orders;
using utb_eshop_patek.Domain.Services.Orders;

namespace utb_eshop_patek.Application.Admin.ApplicationServices.Orders
{
    public class OrderApplicationService : IOrderApplicationService
    {
        private readonly IOrderService _orderService;

        public OrderApplicationService(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public void CreateOrder(int userID, string userTrackingCode)
        {
            _orderService.CreateOrder(userID, userTrackingCode);
        }

        public IndexViewModel GetOrders(int userID)
        {
            var orders = _orderService.GetOrders(userID);
            var orderViewModels = new List<OrderViewModel>();
            foreach (var order in orders)
            {
                orderViewModels.Add(new OrderViewModel
                {
                    OrderItems = order.OrderItems,
                    Total = order.OrderItems.Sum(x => x.Amount * x.Price)
                });
            }
            return new IndexViewModel
            {
                Orders = orderViewModels
            };
        }
    }
}
